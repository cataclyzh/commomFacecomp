<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>


<!DOCTYPE html>
<html lang="zh-CN">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="viewport" content="width=device-width,initial-scale=1,minimum-scale=1,maximum-scale=1,user-scalable=0,minimal-ui">
<title>人脸识别结果</title>
<script src="js/jquery-1.8.2.min.js" type="text/javascript"></script>
<link href="css/base.css" rel="stylesheet" type="text/css" />
<style type="text/css">
	.body {font-family: "Source Han Sans CN Light";}
	.upload_photo{
		text-align: center;
		font-size: 1.5rem; 
		height:20rem;
		line-height:20rem;
	}
	.first{height:70%;}
	.photo{width: 8rem;margin: 4rem auto 1.5rem auto;border-radius: .4rem;border: 1px solid #E8E8E8;overflow: hidden;}
	.upload_action_btn{padding: .4rem 1rem;background: #418BC3;font-size: 1rem;color: #FFF;border: none;border-radius: .3rem}
	.upload_action_btn:hover{background: #118de4}
	.input_id p{margin: 4rem 0 .5rem 0;}
	.input_id input,.btn{-webkit-box-sizing:border-box;width: 90%;padding:.7rem;border:1px solid #E8E8E8;}
	.verification{width: 100%; height:30%;padding: 0.8rem 0; margin-top: 1rem; background-color:#F0F0F0;}
	.btn{display:block; width: 45%; float:left; border-radius: .3rem;border: none;color: #FFF;font-size: 1rem;padding: .5em;margin: 2.5%}
	.c1{background: #FF8D21;}
	.c2{background: #D2D2D2;}
	.andorid_share{display:block; width: 45%; float:right; background: #02BA00;border-radius: .3rem;border: none;color: #FFF;font-size: 1rem;padding: .5em;margin: 2.5%}	
	.iphone_share {display:block; width: 45%; float:right; background: #02BA00;border-radius: .3rem;border: none;color: #FFF;font-size: 1rem;padding: .5em;margin: 2.5%}	
	.verification_result1{margin: 1.5rem 0 2rem 0}
	.verification_result2{margin: 1.7rem 0 0.3rem 0}
	.verification_result1 .percent{font-size: 1.9rem;color: #087D06;}
	.mt0{margin-top: 0}
	.c77BF60{color:  #77BF60;font-size: 1.2rem}
	.comment {font-size: 1rem;color: #045302 ;}
	.ping_fen_ul {margin: 0 auto; height:25px; width:150px}
	.ping_fen {float:left; margin-left: 5px}
	.ping_fen img {width: 25px;}
	.ping_fen_wrapper {margin: 0 auto;}
	.foot{padding-top: 1.3rem; width: 95%; margin: 0 auto;}
</style>
</head>
<body>
<div class="upload_photo">
result=
<%
	String result = request.getParameter("result");
	out.print(result);
%>
${result }
</div>
</body>
</html>
