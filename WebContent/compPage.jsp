<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="zh-CN">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta http-equiv="X-UA-Compatible" content="IE=Edge">
<META HTTP-EQUIV="Pragma" CONTENT="no-cache"> 
<META HTTP-EQUIV="Cache-Control" CONTENT="no-cache"> 
<META HTTP-EQUIV="Expires" CONTENT="0"> 
<meta name="viewport" content="width=device-width,initial-scale=1,minimum-scale=1,maximum-scale=1,user-scalable=0,minimal-ui">
<title>人脸识别</title>
<script src="js/jquery-1.8.2.min.js" type="text/javascript"></script>
<link href="css/base.css" rel="stylesheet" type="text/css" />
<style type="text/css">
	.upload_photo{text-align: center;font-size: 1rem;}
	.photo{
		width: 8rem;
		margin: 0 auto;
		border-radius: .4rem;
		border: 1px solid #E8E8E8;
		overflow: hidden;
	}
	.upload_action_btn{padding: .4rem 1rem;background: #418BC3;font-size: 1rem;color: #FFF;border: none;border-radius: .3rem}
	.upload_action_btn:hover{background: #118de4}
	.input_id p{margin: 1rem 0 .5rem 0;}
	.input_id input,.btn{-webkit-box-sizing:border-box;width: 90%;padding:.7rem;border:1px solid #E8E8E8;}
	.verification{bottom: 0;width: 100%;padding: 1rem 0; margin:0 0 1px 0}
	.btn{height:40px; background: #FF8D21;border-radius: .3rem;border: none;color: #FFF;font-size: 1.0rem;padding: .3em}
	.top {
		padding-top: 10px;
		color: #999999;
	}
	.footer {
		width: 100%;
	}
	.msg {
		margin-top: 5px;
		color:#FF0000;
	}
	.info {
		margin: 30px 0 30px 0;
	}
	.info_line1{
		margin: 5px 5px 30px 5px;
		text-indent: 20px;
		letter-spacing: 0.3pt;
		text-align: center;
		font-size: 1.5rem;
		color:#6B767E;
		width:100%;
		float:right;
	}
	.info_line2{
		margin: 1px 5px 0px 5px;
		text-align: left;		
		font-size: 0.9rem;
		color: #6B767E;
		width:90%;
		float:right;
	}
	.info_line3{
		margin: 1px 5px 0px 5px;
		text-align: left;		
		font-size: 0.9rem;
		color: #6B767E;
		width:90%;
		float:right;
	}
	.info_line4{
		margin: 1px 5px 30px 5px;
		text-align: left;		
		font-size: 0.9rem;
		color: #6B767E;		
		width:90%;
		float:right;
	}
	
	.id_show_default{
		color:#ccc";
	}
	
	.id_show_userinput{
		color: red;
	}
	
	.reUpload{
		margin: 0 auto;
	}
	
	.uploadLink{
		margin: 0 auto;
	}
	
	.info1{
		margin: 5px 0 1px 0;
		color: #73BE5A;
		height: 1rem;
	}
</style>
<%
	String ctx = request.getContextPath();
	
	String error = (String)request.getAttribute("error");
	
	//测试环境
	//String uploadUrlImg = "#uploadImgByClient?imgNum=1&imgSize=200&serverurl=http%3A%2F%2F10.101.2.85%3A8580%2Fface%2FUploadImg%3FidCard%3D"+idCard;
	//发布环境	
	String uploadUrlImg = "#uploadImgByClient?imgNum=1&serverurl=http://58.213.141.220:8888"+ctx+"/UploadImg";
	uploadUrlImg += "&selectPhotoType=onlyCamera";
	
	String pic1 = (String) request.getAttribute("pic1");
	String picSrc = null;
	if (pic1 == null || "".equals(pic1) || "null".equals(pic1)) {
		pic1 = "";
		picSrc = "theme/images/head.png";
	} else {
		picSrc = "upload/" + pic1;
	}
	
	//测试数据
	//pic1 = "2016-01-22/1452702220752.jpg";
%>
<script type="text/javascript">
function uploadImgOver(returns) {
	if (returns == "imgIsNull"){
		showMsg("上传图片内容为空。");
		return;
	}
	if (returns == "uploadsErr"){
		showMsg("上传图片失败。");
		return;
	}
	if (returns.indexOf("ok") == 0) {
		var imgsrc = returns.substr(3);
		document.getElementById("img_1").src= "upload/" + imgsrc;
		document.getElementById("pic_1").value = imgsrc;
		
		//hiddenInfo();
		$(".uploadLink").hide();
		$(".photo").show();
	}
	hiddenMsg();
}

function checkData() {
	var pic = document.getElementById("pic_1").value;
	//var idCard = document.getElementById("idCard").value;
	if (!pic || pic == "") {
		showMsg("请先上传照片。");
		return false;
	} else {
		return true;
	}
}

function showMsg(msg) {
	document.getElementById("msg").innerHTML = msg;
}

function hiddenMsg() {
	document.getElementById("msg").innerHTML = "";
}

function reSetPic(p){
	$("img_1").attr("src", "upload/" + p);
	$(".uploadLink").hide();
	$(".photo").show();
}

$(document).ready(function(){
	$(".photo").hide();
	$(".uploadLink").show();

	<% if (error != null) {%>
		showMsg("<%= error%>");
		reSetPic("<%= pic1%>");
	<% }%>
	
	//test
	//$(".photo").show();
	//$(".uploadLink").hide();
});

</script>
</head>
<body>
<div class="upload_photo">
	<div class="info">
		<p class="info_line1">${title }</p>
		<p class="info_line2">订单号：${orderNumber}</p>
		<p class="info_line4">身份证：${idNumber}</p>
	</div>
	<div class="photo">
		<a href="<%=uploadUrlImg %>">
			<img id="img_1" width="180px" src="<%=picSrc %>">
		</a>
	</div>
	<a class="uploadLink" href="<%=uploadUrlImg %>">
		<img width="180px" src="theme/images/head.png"/><br/>
		<label class="info1"></label>
	</a>
	<form action="facecomp" method="post" onsubmit="return checkData();">
		<input type="hidden" id="pic_1" name="pic1" value="<%=pic1 %>" />
		<input type="hidden" id="idNumber" name="idNumber" value="${idNumber}" />
		<input type="hidden" id="redirectUrl" name="redirectUrl" value="${redirectUrl}" />
		<input type="hidden" id="pic2A" name="pic2A" value="${pic2A}" />
		<input type="hidden" id="pic2B" name="pic2B" value="${pic2B}" />
		<input type="hidden" id="orderNumber" name="orderNumber" value="${orderNumber}" />
		<input type="hidden" id="callingId" name="callingId" value="${callingId}" />
		<input type="hidden" id="title" name="title" value="${title}" />
		<p id="msg" class="msg"></p>
		<div class="verification"><input type="submit" class="btn"  value="开始识别 "/></div>
	</form>
	<div class="footer"><img width="60%"  src="theme/images/footer.png"></div>
</div>
</body>
</html>
