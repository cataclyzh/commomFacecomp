package commonFacecomp;

import static org.junit.Assert.*;

import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.codec.CharEncoding;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import com.fnst.common.facecomp.encypt.EncryptMethodDesUrl;

public class EncryptTest {
	
	static String key = "MyNJ930R";
	
	static String algorithm = "des";

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}

	@Before
	public void setUp() throws Exception {
	}

	@Test
	public void test1() {
		String msg = "";
		
        EncryptMethodDesUrl EncryptMethodDesUrl = new EncryptMethodDesUrl();
        List params = new ArrayList();
        params.add(msg);
        params.add(key);// 密钥
        params.add("des");// 加密方式为des
        EncryptMethodDesUrl.decrypt(params);
	}
	
	@Test
	public void test2() {
		String msg = "人脸识别 订单号 http://123aaaaa.ddd?%3d&rrr";
		String encryptStr = EncryptMethodDesUrl.encrypt(msg, key, algorithm);
		System.out.println("encryptStr: " + encryptStr);
		String decryptStr = EncryptMethodDesUrl.decrypt(encryptStr, key, algorithm);
		System.out.println("decryptStr: " + decryptStr);
		
	}
	
	@Test
	public void test3(){
//		System.out.println(EncryptMethodDesUrl.encrypt("可变动标题可变动标题", key, algorithm));
//		System.out.println(EncryptMethodDesUrl.encrypt("XXXXXXXX1", key, algorithm));
//		System.out.println(EncryptMethodDesUrl.encrypt("320103198706080563", key, algorithm));
//		System.out.println(EncryptMethodDesUrl.encrypt("http://10.101.2.85:8888/commonFacecomp/result.jsp", key, algorithm));
//		System.out.println(EncryptMethodDesUrl.encrypt("caller01", key, algorithm));
		System.out.println(EncryptMethodDesUrl.encrypt("123.45", key, algorithm));
		System.out.println(EncryptMethodDesUrl.encrypt("param1tset", key, algorithm));
		System.out.println(EncryptMethodDesUrl.encrypt("param2tset", key, algorithm));
	}
	
	@Test
	public void test_decryptStr() throws Exception{
		String key = "Mjd90okk";
		String s1 = "Fc6voSO7VuT3I2ug/86+0A==";  
		
		String s2 = URLEncoder.encode(s1, CharEncoding.UTF_8);
		System.out.println(s2);
		
		String decryptStr = EncryptMethodDesUrl.decrypt(s2, key, "des");
		System.out.println("decryptStr: " + decryptStr);
	}

	@Test
	public void test_encryptStr(){
		
	}
}
