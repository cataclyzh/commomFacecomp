package commonFacecomp;

import static org.junit.Assert.*;

import java.io.UnsupportedEncodingException;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;

import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import com.fnst.common.facecomp.utils.Utils;

public class Test1 {

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}

	@Before
	public void setUp() throws Exception {
	}

	@Test
	public void test1() {
		Map m = new HashMap();
		m.put("result", "02");
		m.put("errorMsg", "orderNumber null or empty");
		String result = Utils.changeMapToJSONString(m);
		System.out.println(result);
	}

	@Test
	public void test2(){
		System.out.println(isCallingIdCorrect("JKNJ"));
	}
	
	private  boolean isCallingIdCorrect(String callingId){
		HashSet caller = new HashSet();
		caller.add("JKNJ");
		return caller.contains(callingId);
//		return callingId.equals("caller01");
	}
	
	@Test
	public void test3() throws UnsupportedEncodingException{
		String t1 = "http%3A%2F%2Fmhos.jiankang51.cn%2Fwdnj%2Fregister_result%3Ftarget%3DbeginPay%26refresh%3Dyes%26transData%3D%257B%2522hospitalCode%2522%253A%252280415%2522%252C%2522userId%2522%253A%2522320581199306082338%2522%252C%2522scheduFlow%2522%253A%25220126f19e2f2b4277ab3d237f8cdf026a%2522%252C%2522oprUserId%2522%253A%2522320581199306082338%2522%252C%2522seeTime%2522%253A%2522%25E4%25B8%258B%25E5%258D%2588%2522%252C%2522sectionId%2522%253A%2522%2522%252C%2522cardNo%2522%253A%25221881830251%2522%252C%2522cardNoType%2522%253A%25221%2522%252C%2522bVerify%2522%253A1%252C%2522rcptStreamNo%2522%253A%2522C0020080415201602181602300062%2522%257D";
		String t2 = java.net.URLDecoder.decode(t1,"utf-8");
		System.out.println(t2);
		
	}
	
	@Test
	public void test4() throws Exception{
		String t1 = "http%3A%2F%2F58.213.141.220%3A8888%2FUploadImg";
		String t2 = java.net.URLDecoder.decode(t1,"utf-8");
		System.out.println(t2);
	}
	
	@Test
	public void test5() throws Exception{
		String t1 = "http://58.213.141.220:8888/UploadImg";
		String t2 = java.net.URLEncoder.encode(t1, "UTF-8");
		System.out.println(t2);
	}
}
