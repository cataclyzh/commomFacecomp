package com.fnst.common.facecomp.dao;

import java.io.Reader;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;

import com.fnst.common.facecomp.model.ComparePhotoInfo;
import com.fnst.common.facecomp.model.T035FacecompOrder;
import com.fnst.common.facecomp.model.T036FacecompCompare;
import com.fnst.common.facecomp.model.T037FacecompRecordResult;
import com.ibatis.common.resources.Resources;
import com.ibatis.sqlmap.client.SqlMapClient;
import com.ibatis.sqlmap.client.SqlMapClientBuilder;

public class CommonDao {
	protected Logger log = Logger.getLogger(getClass());
	
	protected static SqlMapClient sqlMapClient;
	
	private static CommonDao instance;
	
	public static synchronized CommonDao getInstance() throws Exception{
		if(instance == null){
			instance = new CommonDao();
		}
		return instance;
	}
	
	public CommonDao() throws Exception{
		Reader reader = Resources.getResourceAsReader ("SqlMapConfig.xml");
		sqlMapClient = SqlMapClientBuilder.buildSqlMapClient(reader);
	}
	
	public void addT035FacecompOrder(T035FacecompOrder facecompOrder){
		try {
			long id = Long.valueOf(sqlMapClient.queryForObject("face.seq_t_035_facecomp_order").toString());
			facecompOrder.setId(id);			
			sqlMapClient.insert("face.insert_t_035_facecomp_order", facecompOrder);
		} catch (Exception e) {
			log.error(e.getMessage(), e);
			facecompOrder.setId(0);
		}
	}
	
	public List<T035FacecompOrder> selectT035FacecompOrderByOrderNumber(String orderNumber){
		List<T035FacecompOrder> result = null;
		try{
			result = sqlMapClient.queryForList("face.selectT035FacecompOrderByOrderNumber", orderNumber);
		}catch(Exception e){
			log.error(e.getMessage(), e);
		}
		return result;
	}
	
	public void addT036FacecompCompare(T036FacecompCompare facecompCompare){
		try {
			long id = Long.valueOf(sqlMapClient.queryForObject("face.seq_t_036_facecomp_compare").toString());
			facecompCompare.setId(id);			
			sqlMapClient.insert("face.insert_t_036_facecomp_compare", facecompCompare);
		} catch (Exception e) {
			log.error(e.getMessage(), e);
			facecompCompare.setId(0);
		}
	}
	
	public List<T035FacecompOrder> selectT036FacecompCompareByOrderNumber(String orderNumber){
		List<T035FacecompOrder> result = null;
		try{
			result = sqlMapClient.queryForList("face.selectT036FacecompCompareByOrderNumber", orderNumber);
		}catch(Exception e){
			log.error(e.getMessage(), e);
		}
		return result;
	}
	
	//
	public void updateAddComment(ComparePhotoInfo c) throws Exception{
		sqlMapClient.update("face.update_addComment", c);
	}
	
	public void updateShareInfo(ComparePhotoInfo c) throws Exception{
		sqlMapClient.update("face.update_addShareInfo", c);
	}
	
	public void updateComparePhotoInfo(ComparePhotoInfo c){
		try{
			sqlMapClient.update("face.update_ComparePhotoInfo", c);
		}catch(Exception e){
			log.error("updateComparePhotoInfo error", e);
		}
	}
	
	public long getUsedTimes(String userCard, String date) throws Exception{
		Map m = new HashMap();
		m.put("userCard", userCard);
		m.put("date", date);
		return Long.valueOf(sqlMapClient.queryForObject("face.getUsedTimes", m).toString());
	}

	public void addT037FacecompRecordResult(T037FacecompRecordResult recordResult) throws Exception{
		sqlMapClient.insert("face.insert_t_037_facecomp_recordResult", recordResult);
	}
}
