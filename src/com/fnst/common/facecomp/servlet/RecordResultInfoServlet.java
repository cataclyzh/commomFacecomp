package com.fnst.common.facecomp.servlet;

import java.io.IOException;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.Servlet;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.fnst.common.facecomp.encypt.EncryptMethodDesUrl;
import com.fnst.common.facecomp.model.T035FacecompOrder;
import com.fnst.common.facecomp.model.T037FacecompRecordResult;
import com.fnst.common.facecomp.utils.Utils;

/**
 * Servlet implementation class RecordResultInfoServlet
 */
public class RecordResultInfoServlet extends AbstractDetectServlet implements Servlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see AbstractDetectServlet#AbstractDetectServlet()
     */
    public RecordResultInfoServlet() {
        super();
    }
    
	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		log.info("=== record result ===");
		
		T037FacecompRecordResult t037 = new T037FacecompRecordResult();
		t037.setInsertTime(new Date());
		
		String orderNumber = request.getParameter("orderNumber");
		String idNumber = request.getParameter("idNumber");
		String patientCard = request.getParameter("patientCard");
		String payType = request.getParameter("payType");
		String orderAmount = request.getParameter("orderAmount");
		String cashAmount = request.getParameter("cashAmount");
		String callingId = request.getParameter("callingId");
		
		log.info("orderNumber: " + orderNumber);
		log.info("idNumber: " + idNumber);
		log.info("patientCard: " + patientCard);
		log.info("payType: " + payType);
		log.info("orderAmount: " + orderAmount);
		log.info("cashAmount: " + cashAmount);
		log.info("callingId: " + callingId);
		
		if(orderNumber == null || orderNumber.length() == 0){
			Map resultMap = new HashMap();
			resultMap.put("result", "orderNumber empty");
			Utils.responseTextFromMap(response, resultMap);
		}
		
		if(callingId == null || callingId.length() == 0){
			Map resultMap = new HashMap();
			resultMap.put("result", "callingId empty");
			Utils.responseTextFromMap(response, resultMap);
		}
		
		t037.setOrderNumber(EncryptMethodDesUrl.decrypt(orderNumber, ENC_KEY, ENC_ALGORITHM));
		t037.setCallingId(EncryptMethodDesUrl.decrypt(callingId, ENC_KEY, ENC_ALGORITHM));

		if(idNumber != null && idNumber.length() > 0){
			t037.setIdNumber(EncryptMethodDesUrl.decrypt(idNumber, ENC_KEY, ENC_ALGORITHM));
		}
		
		if(patientCard != null && patientCard.length() > 0){
			t037.setPatientCard(EncryptMethodDesUrl.decrypt(patientCard, ENC_KEY, ENC_ALGORITHM));
		}
		
		if(payType != null && payType.length() > 0){
			t037.setPayType(EncryptMethodDesUrl.decrypt(payType, ENC_KEY, ENC_ALGORITHM));
		}

		if(orderAmount != null && orderAmount.length() > 0){
			t037.setOrderAmount(EncryptMethodDesUrl.decrypt(orderAmount, ENC_KEY, ENC_ALGORITHM));
		}
		
		if(cashAmount != null && cashAmount.length() > 0){
			t037.setCashAmount(EncryptMethodDesUrl.decrypt(cashAmount, ENC_KEY, ENC_ALGORITHM));
		}
		
		try {
			commonDao.addT037FacecompRecordResult(t037);
		} catch (Exception e) {
			log.error("保存数据失败", e);
		}
		
		Map resultMap = new HashMap();
		resultMap.put("result", "ok");
		String jsonResult = Utils.changeMapToJSONString(resultMap);
		log.info("result: " + jsonResult);
		Utils.responseTextFromMap(response, resultMap);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doGet(request, response);
	}

}
