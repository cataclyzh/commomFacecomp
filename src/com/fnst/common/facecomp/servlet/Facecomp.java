package com.fnst.common.facecomp.servlet;

import java.io.IOException;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.fnst.common.facecomp.exception.ParamException;
import com.fnst.common.facecomp.model.PhotoDate;
import com.fnst.common.facecomp.model.T035FacecompOrder;
import com.fnst.common.facecomp.paramcheck.CallingIdParamCheck;
import com.fnst.common.facecomp.paramcheck.ExtractParamCheck;
import com.fnst.common.facecomp.paramcheck.IdNumberParamCheck;
import com.fnst.common.facecomp.paramcheck.OrderNumberParamCheck;
import com.fnst.common.facecomp.paramcheck.RedirectUrlParamCheck;
import com.fnst.common.facecomp.paramcheck.TitleParamCheck;
import com.fnst.common.facecomp.service.GetPhotoService;
import com.fnst.common.facecomp.utils.Utils;

/**
 * Servlet implementation class facecomp
 */
public class Facecomp extends AbstractDetectServlet {
	private static final long serialVersionUID = 1L;
	
	private GetPhotoService gps = new GetPhotoService();
	
	private TitleParamCheck titleParamCheck = new TitleParamCheck();
	
	private OrderNumberParamCheck orderNumberParamCheck = new OrderNumberParamCheck();
	
	private IdNumberParamCheck idNumberParmCheck = new IdNumberParamCheck();
	
	private RedirectUrlParamCheck redirectUrlParamCheck = new RedirectUrlParamCheck();
	
	private CallingIdParamCheck callingIdParamCheck = new CallingIdParamCheck();
	
	private ExtractParamCheck extractParamCheck = new ExtractParamCheck();
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public Facecomp() {
        super();
    }
	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		log.info("=== facecomp ===");
		T035FacecompOrder t035 = new T035FacecompOrder();
		t035.setInsertTime(new Date());
		
		try{
			//订单金额,备用参数1,备用参数2			
			t035.setOrderAmount(extractParamCheck.checkoutValue(request.getParameter("orderAmount")));
			t035.setParam1(extractParamCheck.checkoutValue(request.getParameter("param1")));
			t035.setParam2(extractParamCheck.checkoutValue(request.getParameter("param2")));

			//标题参数,订单号,身份证,回调地址,调用方识别号
			t035.setRedirectUrl(redirectUrlParamCheck.checkoutValue(request.getParameter("redirectUrl")));
			t035.setTitle(titleParamCheck.checkoutValue(request.getParameter("title")));
			t035.setOrderNumber(orderNumberParamCheck.checkoutValue(request.getParameter("orderNumber")));
			t035.setIdNumber(idNumberParmCheck.checkoutValue(request.getParameter("idNumber")));			
			t035.setCallingId(callingIdParamCheck.checkoutValue(request.getParameter("callingId")));

			//证件照片处理
			PhotoDate photoDate = gps.execute(t035.getIdNumber());
			String daySuffix = Utils.getDateString(new Date()) + "/";
			String idCardPicName1 = getSavePath(daySuffix) + t035.getIdNumber() + "_A.jpg";
			String idCardPicName2 = getSavePath(daySuffix) + t035.getIdNumber() + "_B.jpg";
			Utils.savePicture(photoDate.getPhoto1DateStr(), idCardPicName1);
			Utils.savePicture(photoDate.getPhoto2DateStr(), idCardPicName2);
			if (photoDate.getPhoto1DateStr() == null && photoDate.getPhoto2DateStr() == null) {
				Map m = new HashMap();
				m.put("result", "06");
				m.put("errorMsg", "没有查询到该身份证号码的照片信息");
				throw new ParamException(m);
			}

			//返回参数
			request.setAttribute("title", t035.getTitle());
			request.setAttribute("orderNumber", t035.getOrderNumber());
			request.setAttribute("idNumber", t035.getIdNumber());
			request.setAttribute("redirectUrl", t035.getRedirectUrl());
			request.setAttribute("callingId", t035.getCallingId());
			if(photoDate.getPhoto1DateStr() != null){
				request.setAttribute("pic2A", daySuffix + t035.getIdNumber() + "_A.jpg");
			}
			if(photoDate.getPhoto2DateStr() != null){
				request.setAttribute("pic2B", daySuffix + t035.getIdNumber() + "_B.jpg");
			}

			//记录流程信息
			if(photoDate.getPhoto1DateStr() != null && photoDate.getPhoto2DateStr() == null){
				t035.setPhotoInfo("1");
			}else if(photoDate.getPhoto1DateStr() == null && photoDate.getPhoto2DateStr() != null){
				t035.setPhotoInfo("2");
			}else{
				t035.setPhotoInfo("0");
			}
			t035.setErrorInfo("ok");
			commonDao.addT035FacecompOrder(t035);
		}catch(ParamException e){
			t035.setErrorInfo((String)e.getErrorM().get("result"));
			commonDao.addT035FacecompOrder(t035);
			e.getErrorM().put("result", ParamException.FACECOMP_ERROR);
			sendRedirect(response, t035.getRedirectUrl(), Utils.changeMapToJSONString(e.getErrorM()));
			return;
		}catch(Exception e){
			Map m = new HashMap();
			m.put("result", ParamException.FACECOMP_ERROR);
			m.put("errorMsg", "unexpected error");
			sendRedirect(response, t035.getRedirectUrl(), Utils.changeMapToJSONString(m));
			return;
		}
		
		//上传照片测试数据
		//request.setAttribute("pic1", Utils.getDateString(new Date()) + "/1452702220752.jpg");
		request.setCharacterEncoding("UTF-8");
		request.getRequestDispatcher(PAGE_COMPARE).forward(request, response);
	}
	
	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doGet(request, response);
	}
}
