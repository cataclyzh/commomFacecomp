package com.fnst.common.facecomp.servlet.ot;

import java.io.IOException;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;

import com.fnst.common.facecomp.sign.JsSDKSign;
import com.fnst.common.facecomp.sign.TokenService;
import com.fnst.common.facecomp.utils.Utils;

/**
 * Servlet implementation class GetJsSDKConfig
 */
public class GetJsSDKConfigServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
	protected Logger logger = Logger.getLogger(getClass());
	
    /**
     * @see HttpServlet#HttpServlet()
     */
    public GetJsSDKConfigServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doPost(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		try {
			TokenService tService = TokenService.getInstance();
			String targetUrl = request.getParameter("targetUrl");
			if(targetUrl == null || targetUrl.length() == 0){
				logger.error("targetUrl is null.");
				return;
			}
			
			String ticket = tService.getTicket();
			logger.info("jsapiTicket: " + ticket);
			logger.info("targetUrl: " + targetUrl);
			Map<String, String> ret = JsSDKSign.sign(ticket, targetUrl);
			ret.put("appId", TokenService.APPID);			
			logger.info("response map: " + ret);
	        Utils.responseTextFromMap(response, ret);
		} catch (Exception e) {
			logger.error("验证服务出错", e);
		}
	}

}
