package com.fnst.common.facecomp.servlet.ot;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fnst.common.facecomp.dao.CommonDao;
import com.fnst.common.facecomp.model.ComparePhotoInfo;

/**
 * Servlet implementation class CommentServlet
 */
public class CommentServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	
	protected Logger log = LoggerFactory.getLogger(getClass());
	private CommonDao commonDao;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public CommentServlet() {
    	super();
        try {
			commonDao = CommonDao.getInstance();
		} catch (Exception e) {
			log.error("Comment Servlet Init error", e);
		}
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String comment = request.getParameter("comment");
		String id = request.getParameter("id");
		log.info("comment: " + comment);
		ComparePhotoInfo c = new ComparePhotoInfo();
		c.setId(Long.valueOf(id));
		c.setUserComment(comment);
		try {
			//commonDao.updateAddComment(c);
		} catch (Exception e) {
			log.error("添加用户评价", e);
		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doGet(request, response);
	}

}
