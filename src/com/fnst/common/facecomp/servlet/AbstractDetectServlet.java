package com.fnst.common.facecomp.servlet;

import java.io.File;
import java.io.IOException;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fnst.common.facecomp.dao.CommonDao;
import com.fnst.common.facecomp.model.T035FacecompOrder;

public class AbstractDetectServlet extends HttpServlet {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	protected Logger log = LoggerFactory.getLogger(getClass());
	protected CommonDao commonDao;
	
	protected static final String PAGE_COMPARE = "compPage.jsp";
	
	public static final String ENC_KEY = "MyNJ930R";
	
	public static final String ENC_ALGORITHM = "des";
	
	public AbstractDetectServlet(){
		super();
        try {
			commonDao = CommonDao.getInstance();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	protected String getUploadPath(String daySuffix) {
		return getSavePath("upload", daySuffix);
	}
	
	protected String getSavePath(String daySuffix) {		
		return getSavePath("save", daySuffix);
	}
	
	private String getSavePath(String pathName, String daySuffix){
		String rootPath=getClass().getResource("").getFile().toString();
		String path = rootPath.substring(0, rootPath.indexOf("WEB-INF")) + pathName + "/" + daySuffix;
		File file = new File(path);
		if(!file.exists()){
			file.mkdir();
		}
		return path;
	}
	
	protected void sendRedirect(HttpServletResponse response,String redirectUrl,String jsonResultStr) throws IOException{
		String responseStr = null;
		if(redirectUrl.contains("?")){
			responseStr = redirectUrl + "&result="+jsonResultStr;
		}else{
			responseStr = redirectUrl + "?result="+jsonResultStr;
		}
		log.error("responseStr: " + responseStr);
		response.sendRedirect(responseStr);
		return;
	}
}
