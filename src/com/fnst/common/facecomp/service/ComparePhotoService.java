package com.fnst.common.facecomp.service;

import java.util.concurrent.Callable;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.FutureTask;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fnst.common.facecomp.model.CompareData;
import com.fnst.common.facecomp.softwise_face360.FaceContrast;

public class ComparePhotoService {
	protected Logger log = LoggerFactory.getLogger(getClass());

	private ExecutorService executor = Executors.newFixedThreadPool(5);
	
	public void execute(CompareData cData){
		log.info("=== id: " + cData.getCardId() + " ===");
		try {
			FutureTask<String> futureTask1 = new FutureTask<String>(
					new ComparePhotoProcess(cData.getCardId(),cData.getComparePhoto(),cData.getIdPhoto1()));
			FutureTask<String> futureTask2 = new FutureTask<String>(
					new ComparePhotoProcess(cData.getCardId(),cData.getComparePhoto(),cData.getIdPhoto2()));
			
			if(cData.getIdPhoto1() != null && cData.getIdPhoto2() != null){
				executor.submit(futureTask1);
				executor.submit(futureTask2);
				cData.setResult1(futureTask1.get());
				cData.setResult2(futureTask2.get());
			}else if (cData.getIdPhoto1() != null){
				executor.submit(futureTask1);
				cData.setResult1(futureTask1.get());
				cData.setResult2("0");
			}else if (cData.getIdPhoto2() != null){
				executor.submit(futureTask2);
				cData.setResult1("0");
				cData.setResult2(futureTask2.get());
			}else{
				cData.setResult1("0");
				cData.setResult2("0");
			}

			cData.analysisResult();
		} catch (Exception e) {
			log.error("对比接口错误", e);
			cData.setResult("-1");
			cData.setErrorMsg("对比接口错误");
		}
	}
	
}

class ComparePhotoProcess implements Callable<String>{
	
	private String idCard;
	
	private String comparePhoto;
	
	private String idPhoto;
	
	public ComparePhotoProcess(String idCard, String comparePhoto, String idPhoto){
		this.idCard = idCard;
		this.comparePhoto = comparePhoto;
		this.idPhoto = idPhoto;
	}

	@Override
	public String call() throws Exception {
		return (new FaceContrast()).imgContrast(idCard, comparePhoto, idPhoto);
	}
	
}
