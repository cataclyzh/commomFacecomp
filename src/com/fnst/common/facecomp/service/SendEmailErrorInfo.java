package com.fnst.common.facecomp.service;

import java.util.Date;
import java.util.Properties;

import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSenderImpl;

import com.fnst.common.facecomp.utils.Utils;

public class SendEmailErrorInfo {
	private String rememberDay;
	
	private int errorTimes;
	
	public void resetErrorTimes(){
		errorTimes = 0;
	}
	
	public void resetRememberDay(){
		rememberDay = null;
	}
	
	public void errorExecute(String title, String msg){		
		errorTimes++;
		if(errorTimes >= 10 && !isSendedToday()){
			rememberDay = Utils.getDateString(new Date());
			resetErrorTimes();
			sendMail(msg, title);
		}
	}
	
	public boolean isSendedToday(){
		if(rememberDay == null){
			return false;
		}
		String strDay = Utils.getDateString(new Date());
		if(rememberDay.equals(strDay)){
			return true;
		}else{
			return false;
		}
	}
	
	public void sendMail(String msg, String title){
		try{
			JavaMailSenderImpl senderImpl = new JavaMailSenderImpl();
			senderImpl.setHost("smtp.163.com");// 设定smtp邮件服务器
			SimpleMailMessage mailMessage = new SimpleMailMessage(); 
			mailMessage.setFrom("cataclyzh@163.com");
			mailMessage.setSubject(title);
			mailMessage.setText(msg);
			senderImpl.setUsername("cataclyzh"); // 根据自己的情况,设置username
			senderImpl.setPassword("hero2111"); // 根据自己的情况, 设置password
			Properties prop = new Properties();
			prop.put("mail.smtp.auth", "true"); // 将这个参数设为true，让服务器进行认证,认证用户名和密码是否正确
			prop.put("mail.smtp.timeout", "25000");
			senderImpl.setJavaMailProperties(prop);
			// 发送邮件
			mailMessage.setTo("2716313167@qq.com");
			senderImpl.send(mailMessage);
			System.out.println("send OK by lkmgydx");
		}catch(Exception e){
			e.printStackTrace();
		}
	}
	
	public static void main(String[] args){
		SendEmailErrorInfo seei = new SendEmailErrorInfo();
//		seei.sendMail("测试内容","测试标题");
		for(int i=0; i<30; i++){
			System.out.println(i);
			seei.errorExecute("取证件接口出错", "");
		}
	}
}
