package com.fnst.common.facecomp.service;

import java.util.List;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.FutureTask;

import javax.xml.namespace.QName;
import javax.xml.rpc.ParameterMode;

import org.apache.axis.Constants;
import org.apache.axis.client.Call;
import org.apache.axis.client.Service;
import org.apache.axis.encoding.XMLType;
import org.apache.axis.message.MessageElement;
import org.apache.axis.types.Schema;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fnst.common.facecomp.model.PhotoDate;

public class GetPhotoService{
	protected Logger log = LoggerFactory.getLogger(getClass());

	private ExecutorService executor = Executors.newFixedThreadPool(5);
	
	public static final Integer TIMEOUT = 8000;
	
	private SendEmailErrorInfo sendEmailErrorInfo = new SendEmailErrorInfo();
	
	public PhotoDate execute(String id){
		log.info("id: " + id);
		PhotoDate pDate = new PhotoDate();
		FutureTask<String> futureTask1 = new FutureTask<String>(new GetPic1Process(id));
		FutureTask<String> futureTask2 = new FutureTask<String>(new GetPic2Process(id));
		executor.submit(futureTask1);
		executor.submit(futureTask2);
		
		try {
			pDate.setPhoto1DateStr(futureTask1.get());
		} catch (Exception e) {
			log.error("照片接口错误1: "+e.getMessage(), e);
			sendEmailErrorInfo.errorExecute("facecomp取证件接口出错1", e.getMessage());
		}
		
		try {
			pDate.setPhoto2DateStr(futureTask2.get());
		} catch (Exception e) {
			log.error("照片接口错误1: "+e.getMessage(), e);
			sendEmailErrorInfo.errorExecute("facecomp取证件接口出错2", e.getMessage());
		}
		
		sendEmailErrorInfo.resetErrorTimes();
		sendEmailErrorInfo.resetRememberDay();
		return pDate;
	}
}

class GetPic1Process implements Callable<String>{
	protected Logger log = LoggerFactory.getLogger(getClass());
	private String idCard;
	
	public GetPic1Process(String idCard){
		this.idCard = idCard;
	}

	@Override
	public String call() throws Exception {
		String result = null;
		String method = "queryExecute";
		String nameSpace = "http://rkk.hoperun.com.cn/DataShareService";
		String serviceUrl = "http://10.250.90.200:8080/RkkService/services/DataShareService_GA";
		Service service = new Service();
		Call call = (Call) service.createCall();
		call.setTimeout(GetPhotoService.TIMEOUT);
		call.setTargetEndpointAddress(serviceUrl);
		call.setOperation(method);
		call.setUseSOAPAction(true);
		call.setSOAPActionURI(nameSpace + method);
		call.setOperationName(new QName(nameSpace, method));
		call.setReturnClass(org.w3c.dom.Element.class);
		call.setReturnType(XMLType.XSD_SCHEMA);
		call.addParameter("username", Constants.XSD_STRING, ParameterMode.IN);
		call.addParameter("password", Constants.XSD_STRING, ParameterMode.IN);
		call.addParameter("returnColumn", Constants.XSD_STRING, ParameterMode.IN);
		call.addParameter("whereStr", Constants.XSD_STRING, ParameterMode.IN);
		Schema obj = (Schema) call.invoke(new Object[] {"?", "?", "sfzh,xm,xb,mz,zp", "sfzh='"+idCard+"'"});
		MessageElement[] meArray = obj.get_any();
		if (meArray == null || meArray.length < 3) {
			MessageElement me0 = meArray[0];
			List errorList = me0.getChildren();
			MessageElement mme0 = (MessageElement) errorList.get(1);
			String value = mme0.getValue();
//			log.error(value);
			return value;
		}
		MessageElement me = meArray[2];
		List ll = me.getChildren();
		MessageElement mPic = (MessageElement) ll.get(4);
		result = mPic.getValue(); 
		return result;
	}
}

class GetPic2Process implements Callable<String>{
	protected Logger log = LoggerFactory.getLogger(getClass());
	private String idCard;
	
	public GetPic2Process(String idCard){
		this.idCard = idCard;
	}

	@Override
	public String call() throws Exception {
		String result = null;
		String method = "queryExecute";
		String nameSpace = "http://rkk.hoperun.com.cn/DataShareService";
		String serviceUrl = "http://10.250.90.200:8088/RkkService/services/DataShareService_GA";
		Service service = new Service();
		Call call = (Call) service.createCall();
		call.setTimeout(GetPhotoService.TIMEOUT);
		call.setTargetEndpointAddress(serviceUrl);
		call.setOperation(method);
		call.setUseSOAPAction(true);
		call.setSOAPActionURI(nameSpace + method);
		call.setOperationName(new QName(nameSpace, method));
		call.setReturnClass(org.w3c.dom.Element.class);
		call.setReturnType(XMLType.XSD_SCHEMA);
		call.addParameter("username", Constants.XSD_STRING, ParameterMode.IN);
		call.addParameter("password", Constants.XSD_STRING, ParameterMode.IN);
		call.addParameter("returnColumn", Constants.XSD_STRING, ParameterMode.IN);
		call.addParameter("whereStr", Constants.XSD_STRING, ParameterMode.IN);
		Schema obj = (Schema) call.invoke(new Object[] {"?", "?", "sfzjhm,xm,xb,mz,zp", "sfzjhm='"+idCard+"'"});
		MessageElement[] meArray = obj.get_any();
		if (meArray == null || meArray.length < 3) {
			MessageElement me0 = meArray[0];
			List errorList = me0.getChildren();
			MessageElement mme0 = (MessageElement) errorList.get(1);
			String value = mme0.getValue();
//			log.error(value);
			return value;
		}
		
		MessageElement me = meArray[2];
		List ll = me.getChildren();
		MessageElement mPic = (MessageElement) ll.get(4);
		result = mPic.getValue();
		
		//添加额外的解析
		if(result == null && meArray.length == 4){
			MessageElement me3 = meArray[3];
			List ll3 = me3.getChildren();
			MessageElement mPic3 = (MessageElement) ll3.get(4);
			result = mPic3.getValue();			 
		}
		return result;
	}
}