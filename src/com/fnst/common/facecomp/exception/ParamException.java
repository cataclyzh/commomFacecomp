package com.fnst.common.facecomp.exception;

import java.util.Map;

public class ParamException extends RuntimeException {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	public static final String FACECOMP_ERROR = "-2";
	
	public static final String COMPARE_ERROR = "-1";

	private Map errorM;

	public ParamException(Map errorM) {
		super();
		this.errorM = errorM;
	}

	public Map getErrorM() {
		return errorM;
	}
	
}
