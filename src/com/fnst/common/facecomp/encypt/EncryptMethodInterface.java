/**
 * @Project: znmh_platform
 * @Title: EncryptMethodInterface.java
 * @Package com.hoperun.framework.ssi.freemarker.method
 * @author wu_shenliang1
 * @date 2015年5月12日 
 * @Copyright: © 2015 HopeRun Inc. All rights reserved.
 * @version V1.0  
 */
package com.fnst.common.facecomp.encypt;

import java.util.List;

/**
 * 加密实例类的共同接口
 * @Description TODO
 * @author wu_shenliang1
 * @version 1.0
 */
public interface EncryptMethodInterface
{
    String encrypt(List params);
}
