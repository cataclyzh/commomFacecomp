package com.fnst.common.facecomp.paramcheck;

import com.fnst.common.facecomp.encypt.EncryptMethodDesUrl;

public class ExtractParamCheck extends AbstractParamCheck {
	public ExtractParamCheck(){
		super();
	}
	
	@Override
	public String checkoutValue(String param) {
		String result = null;
		if(param != null && param.trim().length() > 0){
			result = EncryptMethodDesUrl.decrypt(param, ENC_KEY, ENC_ALGORITHM);			
		}
    	return result;
	}
}
