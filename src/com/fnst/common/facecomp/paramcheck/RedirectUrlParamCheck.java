package com.fnst.common.facecomp.paramcheck;

import java.io.UnsupportedEncodingException;
import java.util.HashMap;
import java.util.Map;

import com.fnst.common.facecomp.encypt.EncryptMethodDesUrl;
import com.fnst.common.facecomp.exception.ParamException;

public class RedirectUrlParamCheck extends AbstractParamCheck {
	public RedirectUrlParamCheck(){
		super();
		Map m = new HashMap();
		m.put("result", "-1");
		m.put("errorMsg", "redirectUrl null or empty");
		setErrorMap(m);
	}

	@Override
	public String checkoutValue(String param) {
		String result = null;
		checkNull(param);
    	result = EncryptMethodDesUrl.decrypt(param, ENC_KEY, ENC_ALGORITHM);
    	try {
			result = java.net.URLDecoder.decode(result,"utf-8");
		} catch (UnsupportedEncodingException e) {
			throw new ParamException(getErrorMap());
		}
    	return result;
	}
	
	
}
