package com.fnst.common.facecomp.paramcheck;

import java.util.HashMap;
import java.util.Map;

public class TitleParamCheck extends AbstractParamCheck {
	public TitleParamCheck(){
		super();
		Map m = new HashMap();
		m.put("result", "-1");
		m.put("errorMsg", "title error");
		setErrorMap(m);
	}
}
