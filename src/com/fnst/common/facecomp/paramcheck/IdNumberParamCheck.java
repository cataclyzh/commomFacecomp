package com.fnst.common.facecomp.paramcheck;

import java.util.HashMap;
import java.util.Map;

import com.fnst.common.facecomp.encypt.EncryptMethodDesUrl;
import com.fnst.common.facecomp.exception.ParamException;
import com.fnst.common.facecomp.utils.Utils;

public class IdNumberParamCheck extends AbstractParamCheck {
	public IdNumberParamCheck(){
		super();
		Map m = new HashMap();
		m.put("result", "-1");
		m.put("errorMsg", "idNumber null or empty");
		setErrorMap(m);
	}
	
	@Override
	public String checkoutValue(String param) {
		String result = null;
		checkNull(param);
    	result = EncryptMethodDesUrl.decrypt(param, ENC_KEY, ENC_ALGORITHM);
    	//验证身份证
    	if(!Utils.isIdCorrect(result)){
    		Map m = new HashMap();
    		m.put("result", "-1");
    		m.put("errorMsg", "非法身份证号");
    		throw new ParamException(m);
    	}
    	return result;
	}
}
