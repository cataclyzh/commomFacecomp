package com.fnst.common.facecomp.paramcheck;

import java.util.Map;

import com.fnst.common.facecomp.dao.CommonDao;
import com.fnst.common.facecomp.encypt.EncryptMethodDesUrl;
import com.fnst.common.facecomp.exception.ParamException;
import com.fnst.common.facecomp.servlet.AbstractDetectServlet;

abstract public class AbstractParamCheck {
	private Map errorMap; 
	
	protected CommonDao commonDao;
	
	public AbstractParamCheck(){
		try {
			commonDao = CommonDao.getInstance();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	protected String ENC_KEY = AbstractDetectServlet.ENC_KEY;
	protected String ENC_ALGORITHM = AbstractDetectServlet.ENC_ALGORITHM;
	
	public Map getErrorMap() {
		return errorMap;
	}

	protected void setErrorMap(Map errorMap) {
		this.errorMap = errorMap;
		this.errorMap.put("result", "-1");
	}

	protected void checkNull(String param){
		if(param == null || param.trim().length() == 0){
			throw new ParamException(getErrorMap());
		}		
	}
	
	public String checkoutValue(String param) {
		String result = null;
		checkNull(param);
    	result = EncryptMethodDesUrl.decrypt(param, ENC_KEY, ENC_ALGORITHM); 
    	return result;
	}
	
}
