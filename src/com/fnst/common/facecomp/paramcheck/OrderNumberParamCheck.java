package com.fnst.common.facecomp.paramcheck;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.fnst.common.facecomp.encypt.EncryptMethodDesUrl;
import com.fnst.common.facecomp.exception.ParamException;

public class OrderNumberParamCheck extends AbstractParamCheck {
	public OrderNumberParamCheck(){
		super();
		Map m = new HashMap();
		m.put("result", "-1");
		m.put("errorMsg", "orderNumber null or empty");
		setErrorMap(m);
	}
	
	@Override
	public String checkoutValue(String param) {
		String result = null;
		checkNull(param);
    	result = EncryptMethodDesUrl.decrypt(param, ENC_KEY, ENC_ALGORITHM);
    	//订单号码规则验证
		if(!isOrderNumberCorrect(result)){
			Map m = new HashMap();
			m.put("result", "-1");
			m.put("errorMsg", "订单号不符合规则");
			throw new ParamException(m);
		}
		//订单号唯一性验证
		List orderList = commonDao.selectT035FacecompOrderByOrderNumber(result);
		if(orderList != null && orderList.size() > 0){
			Map m = new HashMap();
			m.put("result", "-1");
			m.put("errorMsg", "订单号重复请求");
			throw new ParamException(m);
		}
    	return result;
	}
	
	public boolean isOrderNumberCorrect(String orderNumber){
		return true;
	}
}
