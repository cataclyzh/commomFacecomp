package com.fnst.common.facecomp.paramcheck;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;

import com.fnst.common.facecomp.encypt.EncryptMethodDesUrl;
import com.fnst.common.facecomp.exception.ParamException;

public class CallingIdParamCheck extends AbstractParamCheck {
	public CallingIdParamCheck(){
		super();
		Map m = new HashMap();
		m.put("result", "-1");
		m.put("errorMsg", "callingId null or empty");
		setErrorMap(m);
	}
	
	private boolean isCallingIdCorrect(String callingId){
		if(callingId == null){
			return false;
		}
		HashSet caller = new HashSet();
		caller.add("JKNJ");
		return caller.contains(callingId);
//		return callingId.equals("caller01");
	}
	
	@Override
	public String checkoutValue(String param) {
		String result = null;
		checkNull(param);
    	result = EncryptMethodDesUrl.decrypt(param, ENC_KEY, ENC_ALGORITHM);
    	//验证调用方id
    	if(!isCallingIdCorrect(result)){
    		Map m = new HashMap();
    		m.put("result", "-1");
    		m.put("errorMsg", "调用方id验证失败");
    		throw new ParamException(m);
    	}
    	return result;
	}
	
	
}
