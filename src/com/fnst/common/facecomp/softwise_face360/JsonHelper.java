package com.fnst.common.facecomp.softwise_face360;

import java.util.ArrayList;
import java.util.List;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonParser;

public class JsonHelper {
	public static <T> T json2Object(String jsonStr, Class<T> c) {
		 Gson gson = new Gson();
		 T t = gson.fromJson(jsonStr, c);
		 return t;
	}

	public static <T> List<T> json2Array(String jsonStr, Class<T> c) {
		Gson gson = new Gson();
		JsonParser parser = new JsonParser();
		JsonArray Jarray = parser.parse(jsonStr).getAsJsonArray();
		ArrayList<T> lcs = new ArrayList<T>();

		for (JsonElement obj : Jarray) {
			T cse = gson.fromJson(obj, c);
			lcs.add(cse);
		}

		return lcs;
	}
}
