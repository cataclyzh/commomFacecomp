package com.fnst.common.facecomp.model;

public class ComparePhotoNumber {
	private long id;
	private long totalNumber;
	private long currentDayNumber;
	private String day;
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	public long getTotalNumber() {
		return totalNumber;
	}
	public void setTotalNumber(long totalNumber) {
		this.totalNumber = totalNumber;
	}
	public long getCurrentDayNumber() {
		return currentDayNumber;
	}
	public void setCurrentDayNumber(long currentDayNumber) {
		this.currentDayNumber = currentDayNumber;
	}
	public String getDay() {
		return day;
	}
	public void setDay(String day) {
		this.day = day;
	}
	
	public void incrementNumber(){
		totalNumber++;
		currentDayNumber++;
	}
	
	public boolean isOtherDay(String currentDay){
		return !this.day.equals(currentDay);
	}
}
