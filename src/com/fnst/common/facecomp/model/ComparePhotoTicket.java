package com.fnst.common.facecomp.model;

import java.text.ParseException;
import java.util.Date;

import com.fnst.common.facecomp.utils.Utils;

public class ComparePhotoTicket {
	private long id;
	private String accessToken;
	private String jsapiTicket;
	private String requestTime;
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	public String getAccessToken() {
		return accessToken;
	}
	public void setAccessToken(String accessToken) {
		this.accessToken = accessToken;
	}
	public String getJsapiTicket() {
		return jsapiTicket;
	}
	public void setJsapiTicket(String jsapiTicket) {
		this.jsapiTicket = jsapiTicket;
	}
	public String getRequestTime() {
		return requestTime;
	}
	public void setRequestTime(String requestTime) {
		this.requestTime = requestTime;
	}
	
	public boolean isExpired(){
		try {
			Date ticketDate = Utils.parseTime(requestTime);
			if((System.currentTimeMillis() - ticketDate.getTime()) > 7000 * 1000){
				return true;
			}else{
				return false;
			}
		} catch (ParseException e) {
			return false;
		}
	}
}
