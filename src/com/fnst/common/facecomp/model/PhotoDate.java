package com.fnst.common.facecomp.model;

public class PhotoDate {
	private String cardId;
	private String photo1DateStr;
	private String photo2DateStr;
	public String getCardId() {
		return cardId;
	}
	public void setCardId(String cardId) {
		this.cardId = cardId;
	}
	public String getPhoto1DateStr() {
		return photo1DateStr;
	}
	public void setPhoto1DateStr(String photo1DateStr) {
		this.photo1DateStr = photo1DateStr;
	}
	public String getPhoto2DateStr() {
		return photo2DateStr;
	}
	public void setPhoto2DateStr(String photo2DateStr) {
		this.photo2DateStr = photo2DateStr;
	}
}
