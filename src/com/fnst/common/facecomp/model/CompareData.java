package com.fnst.common.facecomp.model;

public class CompareData {
	private String cardId;
	private String comparePhoto;
	private String idPhoto1;
	private String idPhoto2;
	private String result1;
	private String result2;
	
	private String result;
	private String errorMsg;
	
	public void setResult(String result) {
		this.result = result;
	}
	public void setErrorMsg(String errorMsg) {
		this.errorMsg = errorMsg;
	}
	public String getResult() {
		return result;
	}
	public String getErrorMsg() {
		return errorMsg;
	}
	public String getCardId() {
		return cardId;
	}
	public void setCardId(String cardId) {
		this.cardId = cardId;
	}
	public String getComparePhoto() {
		return comparePhoto;
	}
	public void setComparePhoto(String comparePhoto) {
		this.comparePhoto = comparePhoto;
	}
	public String getIdPhoto1() {
		return idPhoto1;
	}
	public void setIdPhoto1(String idPhoto1) {
		this.idPhoto1 = idPhoto1;
	}
	public String getIdPhoto2() {
		return idPhoto2;
	}
	public void setIdPhoto2(String idPhoto2) {
		this.idPhoto2 = idPhoto2;
	}
	public String getResult1() {
		return result1;
	}
	public void setResult1(String result1) {
		this.result1 = result1;
	}
	public String getResult2() {
		return result2;
	}
	public void setResult2(String result2) {
		this.result2 = result2;
	}
	
	public void analysisResult(){
		float score1 = 0;
		String error1 = null;
		try {			
			score1 = Float.parseFloat(result1);
		} catch (NumberFormatException e) {
			score1 = -1;
			error1 = result1;
		}
		
		float score2 = 0;
		String error2 = null;
		try {			
			score2 = Float.parseFloat(result2);
		} catch (NumberFormatException e) {
			score2 = -1;
			error2 = result2;
		}
		
		if (score1 == -1 && score2 == -1) {
			result="-1";
			if(error1 != null && error1.trim().length() > 0){
				errorMsg = error1;				
			}else{
				errorMsg = error2;
			}
		} else {
			int rate = Math.round(score1 * 100);
			if(score1 < score2){
				rate = Math.round(score2 * 100);
			}
			result = "" + rate;
			errorMsg = "ok";
		}
	}
}
