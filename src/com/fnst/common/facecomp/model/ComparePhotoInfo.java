package com.fnst.common.facecomp.model;

public class ComparePhotoInfo {
	private long id;
	private String userIdentityCard;
	private String compareIdentityCard;
	private String uploadPhoto;
	private String interfacePhoto;
	private String uploadTime;
	private String compareResult; 
	private String compareInfo; // 0 没有比较, 1比较完成, 2没有查询到身份证的照片信息, 3上传图片没有检查出人脸
	private String userComment;
	private String shareInfo;	

	public String getShareInfo() {
		return shareInfo;
	}

	public void setShareInfo(String shareInfo) {
		this.shareInfo = shareInfo;
	}

	public String getUserComment() {
		return userComment;
	}

	public void setUserComment(String userComment) {
		this.userComment = userComment;
	}

	public String getUploadPhoto() {
		return uploadPhoto;
	}

	public void setUploadPhoto(String uploadPhoto) {
		this.uploadPhoto = uploadPhoto;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getUserIdentityCard() {
		return userIdentityCard;
	}

	public void setUserIdentityCard(String userIdentityCard) {
		this.userIdentityCard = userIdentityCard;
	}

	public String getCompareIdentityCard() {
		return compareIdentityCard;
	}

	public void setCompareIdentityCard(String compareIdentityCard) {
		this.compareIdentityCard = compareIdentityCard;
	}

	public String getInterfacePhoto() {
		return interfacePhoto;
	}

	public void setInterfacePhoto(String interfacePhoto) {
		this.interfacePhoto = interfacePhoto;
	}

	public String getUploadTime() {
		return uploadTime;
	}

	public void setUploadTime(String uploadTime) {
		this.uploadTime = uploadTime;
	}

	public String getCompareResult() {
		return compareResult;
	}

	public void setCompareResult(String compareResult) {
		this.compareResult = compareResult;
	}

	public String getCompareInfo() {
		return compareInfo;
	}

	public void setCompareInfo(String compareInfo) {
		this.compareInfo = compareInfo;
	}
}
