package com.fnst.common.facecomp.utils;

public class Constant {
	public static final long MAX_TIMES = 10;
	
//	public static final String SERVER_BASE_URL = "http://10.101.2.85:8580/face/";
	public static final String SERVER_BASE_URL = "http://58.213.141.220:8080/face/";
	
	public static final String SERVER_UPLOAD_URL;
	
	public static final String SHARE_URL;
	
	public static final String FACE_LOGO;
	
	public static final String PAGE_INDEX = "compPage.jsp";
	
	public static final String PAGE_RESULT = "result.jsp";
	
	public static final String SHARE_TITLE = "南京人！全民体验人脸识别";
	
	public static final String SHARE_DESC1 = "我的人脸识别度是：";
	
	public static final String SHARE_DESC2 = "。你的房子会被别人卖了吗？";
	
	//错误信息号
//	public static final String error_info_
	
	static{
		SERVER_UPLOAD_URL = SERVER_BASE_URL + "upload";
		//SHARE_URL = SERVER_BASE_URL + "polulate.html";
		SHARE_URL = "http://id.mynj.cn:8080/face/" + "polulate.html";
		FACE_LOGO = SERVER_BASE_URL + "theme/images/face_logo.png";
	}
}
